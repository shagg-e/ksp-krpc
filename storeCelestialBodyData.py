import krpc
import time
import sqlite3
import os
import math

if not os.path.isfile('CelestialBodyData.db'):
    dbcon = sqlite3.connect(r"CelestialBodyData.db")
    print ('Database created: CelestialBodyData')
else:
    dbcon = sqlite3.connect("CelestialBodyData.db")
    print ('Database already exists: CelestialBodyData')
dbc = dbcon.cursor()



# connects to the kRPC server in the game client
c = krpc.connect(name='storeCelestialBodyData')
print('Connection made to KRPC server...')
time.sleep(1)

# instantiate the dict full of all celestial bodies in game
cbDict = c.space_center.bodies

# set up game clock data stream in ut
sc = c.space_center
utc = c.add_stream(getattr, sc, 'ut')
utc.start()

# create data table for logging UTC with each iteration
# of the database population script


# create sun reference frame for storing position data
# relative to a fixed reference frame around the sun
for a, b in cbDict.items(): # in this dict, a is name of the body and b is the body object
    if str(a) == 'Sun':
        sunRF = b.reference_frame

x = 1  # for a count in the loop
for a, b in cbDict.items():
    if str(a) == 'Sun':  # check if we are at the Sun object in the dict
        print ('3 Sun needs no attributes read at this point in time.')
    else:

        bPos = b.orbit.position_at(utc(), sunRF)

        print (str(x) + ' ' + str(a) + ' is orbiting ' + str(b.orbit.body.name) +
               ' and an altitude of ' + str(round(b.orbit.radius, 2)) + ' meters.' +
               ' and a position of ' + str(bPos))

        # Create table if it doesn't exist already
        command = 'CREATE TABLE IF NOT EXISTS ' + str.lower(str(a)) + ' (id integer PRIMARY KEY, x real, y real, z real)'
        dbc.execute(command)
    x += 1

utc.remove()