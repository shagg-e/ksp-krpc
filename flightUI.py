import time
import krpc
import os

conn = krpc.connect(name='User Interface')
canvas = conn.ui.stock_canvas

# Get the size of the game window in pixels
screen_size = canvas.rect_transform.size

# Add a panel to contain the UI elements
panel = canvas.add_panel()

# Position the panel on the right of the screen
rect = panel.rect_transform
rect.size = (200, 180)
rect.position = ((screen_size[0]/2) - 150, 0 + 350)

# instantiate launch script button
launchButton = panel.add_button("Launch")
launchButton.rect_transform.position = (0, 55)  # place button in center of panel
launchButtonClicked = conn.add_stream(getattr, launchButton, 'clicked')  # create stream for button click

# instantiate hohmann transfer button
hohmannTransferButton = panel.add_button("Hohmann Transfer")
hohmannTransferButton.rect_transform.position = (0, 15)
hTButtonClicked = conn.add_stream(getattr, hohmannTransferButton, 'clicked')

# instantiate circularization button
circButton = panel.add_button("Circularization")
circButton.rect_transform.position = (0, -25)
circButtonClicked = conn.add_stream(getattr, circButton, 'clicked')

# instantiate exec burn button
execButton = panel.add_button("Execute Maneuver")
execButton.rect_transform.position = (0, -65)
execButtonClicked = conn.add_stream(getattr, execButton, 'clicked')



while True:

    if launchButtonClicked():
        os.system('python3 orbital0800.py')  # run the orbital flight script
        # import orbital0800.py
        launchButton.clicked = False  # unclick the button

    if hTButtonClicked():
        os.system('python3 planHohmannTransfer.py')  # run the orbital flight script
        hohmannTransferButton.clicked = False  # unclick the button

    if circButtonClicked():
        os.system('python3 planCircularization.py')
        circButton.clicked = False

    if execButtonClicked():
        os.system('python3 execManeuver.py')
        execButton.clicked = False


    time.sleep(0.1)
