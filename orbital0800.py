
import krpc
import time
from playsound import playsound

# connects to the kRPC server in the game client
conn = krpc.connect(name='Orbital flight')
print('Connection attempted...')
# playsound('./callouts/countdown/connection.wav')
time.sleep(1)

# set these prior to each launch

# whether or not your launcher is set to use boosters
# be aware that this assumes your boosters are set to flame
# out before your core AND that you will be making orbit with
# just the boosters, core, and a single upper stage
hasBoosters = True

# instantiate the object of the current vessel using the conn
vessel = conn.space_center.active_vessel
print('Vessel object instantiated...')
time.sleep(1)


# set autopilot target
ap = vessel.auto_pilot
print('Autopilot instantiated...')
time.sleep(1)
ap.auto_tune = True
ap.time_to_peak = (5, 5, 5)
ap.overshoot = (0.005, 0.005, 0.005)
ap.attenuation_angle = (0.1, 0.1, 0.1)
print('Autopilot tuned...')
# playsound('./callouts/countdown/autopilot.wav')

# lock steering before takeoff
ap.engage()
vessel.auto_pilot.target_pitch_and_heading(90, 90)

# set throttle to 100%
vessel.control.throttle = 1
print('Throttle up...')
# playsound('./callouts/countdown/throttle.wav')
time.sleep(1)

# flight object created
flightObject = vessel.flight()

# # ignite the engines for a few seconds before releasing clamps
# print('Engine ignition!')
# vessel.control.activate_next_stage()
# time.sleep(3)
# print('Liftoff!')

# begin countdown
x = 10
while True:
    if x != 5 and x != 0:
        wavPath = './callouts/countdown/' + str(x) + '.wav'
        # playsound(wavPath, False)
    if x == 5:
        # playsound('./callouts/countdown/MESU.wav', False)
        vessel.control.activate_next_stage()
    if x == 0:
        # playsound('./callouts/countdown/liftoff.wav')
        time.sleep(0.2)
        break
    x -= 1
    time.sleep(1)





# wait some time after liftoff, then start pitching over
curSurfAltitude = conn.add_stream(getattr, flightObject, 'surface_altitude')  # create data stream for surfalt
while curSurfAltitude() < 150:
    time.sleep(0.2)
vessel.auto_pilot.target_roll = 0
while curSurfAltitude() < 1000:
    time.sleep(1)

pitch = 90
currentG = flightObject.g_force # save the value of current accel
diffG = currentG - flightObject.g_force

# needs a proper function call but execute the pitch loop
# until pitch hits the 80 deg marker
# playsound('./callouts/pitchEngaged.wav', False)
while pitch > 84:
    pitch -= 0.5
    vessel.auto_pilot.target_pitch_and_heading(pitch, 90)
    time.sleep(0.1)
    ap.wait()
time.sleep(5)
# ap.disengage()

# set up autopilot for prograde
progradeAP = vessel.auto_pilot
progradeAP.engage()
srf = vessel.surface_velocity_reference_frame
vel = conn.add_stream(getattr, vessel.flight(srf), 'velocity')
progradeAP.reference_frame = srf
progradeAP.target_direction = vel()
# playsound('./callouts/gravTurnInit.wav', False)
time.sleep(20)

while curSurfAltitude() < 30000:
    time.sleep(1)
progradeAP.disengage()
curSurfAltitude.remove()


mainAP = vessel.auto_pilot
mainAP.auto_tune = True
mainAP.time_to_peak = (5, 5, 5)
mainAP.overshoot = (0.005, 0.005, 0.005)
mainAP.attenuation_angle = (0.1, 0.1, 0.1)
mainAP.engage()
print('Autopilot reinitialized for new portion of flight! pausing for 1 second...')
time.sleep(1)

orbitObject = vessel.orbit
curThrust = conn.add_stream(getattr, vessel, 'thrust')
curTimeToAP = conn.add_stream(getattr, orbitObject, 'time_to_apoapsis')
curApoapsis = conn.add_stream(getattr, orbitObject, 'apoapsis_altitude')
curVesselPitch = conn.add_stream(getattr, flightObject, 'pitch')
time.sleep(1)



# after reaching 30km manage time to ap
# playsound('./callouts/gravTurnComplete.wav', False)
while curThrust() > 0:
    maxCurStageThrust = vessel.available_thrust  # save max thrust of current stage
    currentThrust = vessel.thrust
    time.sleep(0.05)
    if currentThrust < maxCurStageThrust * 0.10:

        break

    tempAP = curTimeToAP()
    time.sleep(0.2)
    if curTimeToAP() > 50:
        if (tempAP - curTimeToAP()) > 0:  # if curTimeToAP() is going down
            time.sleep(0.2)
        else:  # if it is going up
            if vessel.control.throttle > 0.1:
                vessel.control.throttle -= 0.02
    else:
        if (tempAP - curTimeToAP()) > 0:  # if curTimeToAP() is going down
            if vessel.control.throttle < 1:
                vessel.control.throttle += 0.02
        else:  # if it is going up
            time.sleep(0.2)
    # add pitch control for runaway apogee
    if curApoapsis() > 150000:
        # pitch control
        if curVesselPitch() > 0:
            mainAP.target_pitch -= 0.25
    time.sleep(0.1)
vessel.control.throttle = 1  # reset the throttle


# booster sep or stage sep depending on hasBoosters
if hasBoosters:
    print('Booster separation engaged!...')
    vessel.control.activate_next_stage()
else:
    print('Stage separation engaged!...')
    mainAP.disengage()
    mainAP.sas = True  # set SAS and hold next autopilot event
    time.sleep(0.5)
    vessel.control.activate_next_stage()
    # playsound('./callouts/stageSep.wav', False)
time.sleep(0.5)
print('Separation complete!...')

mainAP.engage()
mainAP.target_direction = vel()  # retarget ap to prograde

# run this loop to finish the core only if
# the rocket has boosters set up as described
if hasBoosters:
    while vessel.thrust > 0:
        # check for engine cutoff
        maxCurStageThrust = vessel.available_thrust  # save max thrust of current stage
        time.sleep(0.05)
        currentThrust = vessel.thrust
        if currentThrust < maxCurStageThrust * 0.10:
            break
        time.sleep(0.05)
    print('Core separation engaged!...')
    mainAP.disengage()
    mainAP.sas = True  # set SAS and hold next autopilot event
    time.sleep(0.5)
    vessel.control.activate_next_stage()
    time.sleep(0.1)
    print('Separation complete!...')

# hold prograde for a few seconds before engaging terminal guidance
tick = 0
print('Pausing a few seconds to stabilize!...')
while tick < 5:
    time.sleep(1)
    tick += 1


# get list of fairings
fairingList = vessel.parts.fairings
print('List of fairings instantiated for later jettisoning...')
for b in fairingList:
    b.jettison()
# playsound('./callouts/fairingSep.wav')
print('Fairing separation complete!...')


mainAP.disengage()  # disengage AP after using sas mode
terminalAP = vessel.auto_pilot
terminalAP.auto_tune = True
terminalAP.time_to_peak = (5, 5, 5)
terminalAP.overshoot = (0.005, 0.005, 0.005)
terminalAP.attenuation_angle = (0.1, 0.1, 0.1)
terminalAP.engage()
terminalAP.target_pitch = 0
tempAP = curTimeToAP()
time.sleep(0.2)

# set up data streams for terminal guidance
curPeriapsis = conn.add_stream(getattr, orbitObject, 'periapsis_altitude')

# terminal guidance
# playsound('./callouts/terminalguidance.wav', False)
while curPeriapsis() < 150000:

    tempAP = curTimeToAP()
    time.sleep(0.2)

    # early terminal guidance
    if curApoapsis() > 150000 and curTimeToAP() > 30:
        if terminalAP.target_pitch > -10:
            terminalAP.target_pitch -= 0.5
        elif terminalAP.target_pitch < 10:
            terminalAP.target_pitch += 0.5

    # late terminal guidance
    if curTimeToAP() < 30:
        # check to make sure we don't go out of pitch bounds
        # and that time to AP is still going down
        if (terminalAP.target_pitch + 0.5) < 35 and (tempAP - curTimeToAP()) > 0:
            terminalAP.target_pitch += 0.5

        # when time to AP starts going back up
        elif (terminalAP.target_pitch - 0.5) > -10 and (tempAP - curTimeToAP()) < 0:
            terminalAP.target_pitch -= 0.35

# kill engines when peri passes 150km
# playsound('./callouts/seco.wav', False)
vessel.control.throttle = 0
time.sleep(5)
# playsound('./callouts/goodorbit.wav')
