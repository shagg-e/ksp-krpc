import krpc
import time

# create krpc connection
conn = krpc.connect(name="Planning circularization burn...")

# create mechjeb object
mj = conn.mech_jeb

print("Planning circularization burn")
planner = mj.maneuver_planner  # create planner object
circularization = planner.operation_circularize  # create maneuver object
circTimeSelector = circularization.time_selector
circTimeRef = circTimeSelector.time_reference

print(str(type(circTimeRef)) + ' is type of circTimeRef')
print(str(circTimeRef))
circTimeRef = circTimeRef.apoapsis
circTimeSelector.time_reference = circTimeRef
print(str(circTimeRef))

circularization.make_node()  # actually make the maneuver

# check for warnings
# warning = circularization.error_message
# if warning:
#     print(warning)

# create executor object
executor = mj.node_executor
executor.tolerance = 3.0
executor.execute_one_node()  # do the thing