import krpc
import time

# create krpc connection
conn = krpc.connect(name="Planning circularization burn...")

# create mechjeb object
mj = conn.mech_jeb

# execute the next node
executor = mj.node_executor
executor.tolerance = 3.0
executor.execute_one_node()  # do the thing