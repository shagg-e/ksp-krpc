
import krpc
import time

# connects to the kRPC server in the game client
conn = krpc.connect(name='Sub-orbital flight')
print('Connection attempted...')
time.sleep(1)


# instantiate the object of the current vessel using the conn
vessel = conn.space_center.active_vessel
print('Vessel object instantiated...')
time.sleep(1)


# set autopilot target and engage autopilot
vessel.auto_pilot.target_pitch_and_heading(90, 135)
vessel.auto_pilot.engage()
print('Autopilot engaged...')
time.sleep(1)

# set throttle to 100%
vessel.control.throttle = 1
print('Throttle up...')
time.sleep(1)

# ignite the engines for a few seconds before releasing clamps
print('Engine ignition!')
vessel.control.activate_next_stage()

#release the clamps
time.sleep(2)
print('Liftoff!')
print(vessel.control.sas_mode)

# wait some time after liftoff, then start pitching over
time.sleep(10)
pitch = 90 # set the current pitch

# needs a proper function call but execute the pitch loop
# until thrust is zero
while vessel.thrust > 0:
    if pitch > 25:
        pitch = pitch + 0.1
    vessel.auto_pilot.target_pitch_and_heading(pitch, 135)
    time.sleep(0.2)

# stage and pause a second
print('Staging!! Good Fucking Luck')
vessel.control.activate_next_stage()
time.sleep(1)

# run the loop again. should be just a basic
# function call when this is written appropriately
while vessel.thrust > 0:
    if pitch > 25:
        pitch = pitch + 0.1
    vessel.auto_pilot.target_pitch_and_heading(pitch, 135)
    time.sleep(0.2)
