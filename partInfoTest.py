# This script will be entirely devoted to printing some usable information
# about a specific vessel to write further scripts. Specifically, this script
# will print a list of parts and their current actions that is equivalent
# to the actions you see when you right click a part in the flight scene

import krpc
import time

# connects to the kRPC server in the game client
conn = krpc.connect(name='Sub-orbital flight')
print('Connection attempted...')
time.sleep(1)

# instantiate the object of the current vessel using the conn
vessel = conn.space_center.active_vessel
print('Vessel object instantiated...')
time.sleep(1)

# create instance of vessel parts object using current vessel object
vesselParts = vessel.parts

# call the "all" method on the parts object to return
# a list of all parts on the vessel
listOfParts = vesselParts.all


# this loop will make a list of lists.
# each sub-list will be a list of each action
# a given part can take that is equivalent
# to right clicking during flight and choosing
# one of the clickable buttons

for x in range (len(listOfParts)):

    # give title to sub-list by calling current part name
    print('*****' + listOfParts[x].name + '*****')

    # instantiate a list of the part's modules by calling the modules method
    listModules = listOfParts[x].modules

    # execute the following loop for each module in the list of modules
    for y in range (len(listModules)):

        # create a list of actionable events from the current module of the current part
        eventList = listModules[y].events

        # create a tracking int for determining how many iterations
        # of the below for loop have been executed
        zLoopCount = 0

        # execute the following loop for each event in the list of events
        for z in range(len(eventList)):



            # check if the specific item in the list is empty.
            # for some reason there are a ton of empty indexes
            # due to how the game engine handles this in the
            # backend which makes this check necessary
            if eventList[z]:
                print(listOfParts[x].name + ': ' + eventList[z] + ' is in Module index #' + str(y) + ' and action #' + str(zLoopCount) + ' in the list of modules for this specific instance of this given part')
            zLoopCount += 1

    # iterate the main loop forward by one
    x = x + 1

    # make the console output readable by
    # adding space between each sub-list
    print('')
    print('')