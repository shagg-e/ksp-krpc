# This will use a mix of legacy code from previous stuff we've written
# and newly improved logic that will be aimed to be as universal as possible.

import krpc
import time

# boolean for debug prints
debugmode = True


# connect to kRPC client
conn = krpc.connect()
if debugmode:
    print('Connection established')


# these values will be 'trained' and predicted for later on
# so keeping them here for organization
turnstartspd = 85  # likely between 40 and 150
turnangle = 76  # not sure on this one, probably max of 85

# these values will be set by the user in a UI
targetapogee = 185000
targetperigee = 150000
hasboosters = True  # probably can be deprecated

# during late gravity turn, this is time to apo when switch to upper guidance
# higher numbers here for low TWR upper stages
switchtoupper = 60


# creation of misc things
vessel = conn.space_center.active_vessel  # vessel object
srf = vessel.surface_velocity_reference_frame  # for prograde later
ssrf = vessel.orbit.body.reference_frame  # for surface speed like what says on navball in srf mode
flightObject = vessel.flight(srf)  # flight object
orbitObject = vessel.orbit  # orbit object
stagethrottles = True
fairinglist = vessel.parts.fairings  # gets list of payload fairing objects to later use to eject when safe


# create and tune autopilot
ap = vessel.auto_pilot
ap.auto_tune = True
ap.time_to_peak = (5, 5, 5)
ap.overshoot = (0.005, 0.005, 0.005)
ap.attenuation_angle = (0.1, 0.1, 0.1)
ap.roll_threshold = 1
ap.engage()
if debugmode:
    print('Autopilot tuned and engaged')


# initial telemetry
cursurfalt = conn.add_stream(getattr, flightObject, 'surface_altitude')
spd = conn.add_stream(getattr, vessel.flight(ssrf), 'speed')
vel = conn.add_stream(getattr, vessel.flight(srf), 'velocity')
pitch = conn.add_stream(getattr, ap, 'target_pitch')
curapogee = conn.add_stream(getattr, orbitObject, 'apoapsis_altitude')
curperigee = conn.add_stream(getattr, orbitObject, 'periapsis_altitude')
tta = conn.add_stream(getattr, orbitObject, 'time_to_apoapsis')
aoa = conn.add_stream(getattr, flightObject, 'angle_of_attack')
curthrust = conn.add_stream(getattr, vessel, 'thrust')
dynpress = conn.add_stream(getattr, flightObject, 'dynamic_pressure')

# set throttle to 100%
vessel.control.throttle = 1
if debugmode:
    print('Throttle set to 100%')


def main():
    global stagethrottles
    takeoff()
    pitchover()
    if canstagethrottle():
        stagethrottles = True
    else:
        stagethrottles = False
    if debugmode:
        print(stagethrottles)
    gravityturn()
    upperguidance()
    vessel.control.throttle = 0  # target periapsis reached


def takeoff():
    ap.target_pitch_and_heading(90, 90)  # lock for takeoff stability
    while spd() < 10:
        time.sleep(0.5)
    ap.target_roll = 0
    ap.wait()
    while spd() < turnstartspd:
        time.sleep(0.5)
    if debugmode:
        print('turnstartspd reached')


def pitchover():
    spd.remove()
    if debugmode:
        print('Pitch Maneuver Begun')
    while pitch() > turnangle:
        ap.target_pitch -= 0.5
        time.sleep(0.1)
    ap.wait()
    time.sleep(3)


def gravityturn():
    global stagethrottles

    if debugmode:
        print('Engaging Gravity Turn')

    ap.reference_frame = srf
    ap.target_direction = vel()
    ap.target_roll = 0

    while True:

        # break to upper guidance if we stage
        if shoulditstage():
            stagethrottles = canstagethrottle()
            break

        # when most of the way to apo, start managing apo itself
        if targetapogee > curapogee() > (0.45 * targetapogee):
            if stagethrottles:  # useful to throttle down here
                if not vessel.control.throttle == 0.05:
                    vessel.control.throttle = 0.05
            if debugmode:
                print('Managing apogee')
            pitchforapogee()
        if curapogee() > targetapogee:
            overshootapogee()
        if dynpress() < 10:
            for b in fairinglist:
                b.jettison()
        time.sleep(0.5)

        # or break to upper guidance if time to apo falls below
        # target and we're high enough up for it to make sense
        if tta() < switchtoupper and cursurfalt() > (0.5 * targetapogee):
            break


def shoulditstage():

    if debugmode:
        print('Checking if ready to stage!')

    if curthrust() < 5:
        if debugmode:
            print("Fuck it we're doing it live!")
        vessel.control.throttle = 1
        vessel.control.activate_next_stage()
        return True
    else:
        return False


def pitchforapogee():
    if 5 > aoa() > -5 and apogoingup():
        newpitch = pitch() - 3
        time.sleep(1.5)
        ap.target_pitch = newpitch
        ap.wait()
    if apogoingdown():
        newpitch = pitch() + 3
        time.sleep(1.5)
        ap.target_pitch = newpitch
        ap.wait()
    if debugmode:
        print(str(aoa()))


def overshootapogee():
    if 5 > aoa() > -5:
        newpitch = pitch() - 3
        time.sleep(1)
        ap.target_pitch = newpitch
    time.sleep(0.25)


def apogoingup():
    tempapo = curapogee()
    time.sleep(0.1)
    if (tempapo - curapogee()) > 0:
        return False
    else:
        return True


def apogoingdown():
    tempapo = curapogee()
    time.sleep(0.1)
    if (tempapo - curapogee()) < 0:
        return False
    else:
        return True


def upperguidance():
    global stagethrottles

    if debugmode:
        print('Switching to upper guidance')

    while True:

        latepitch = ap.target_pitch
        time.sleep(0.25)

        if shoulditstage():
            # time.sleep(1)
            stagethrottles = canstagethrottle()

        # do this if apo is LESS than target
        if curapogee() < targetapogee:
            tempapo = curapogee()
            time.sleep(0.1)
            if (tempapo - curapogee()) < 0:  # if INCREASING do nothing
                time.sleep(0.001)  # because favorable conds
            else:  # if DECREASING, do stuff to fix
                if vessel.control.throttle == 1:
                    if latepitch < 30:
                        latepitch += 5
                    ap.target_pitch = latepitch
                elif stagethrottles:
                    vessel.control.throttle += 0.05

        # do this if apo is MORE than target
        if curapogee() > targetapogee:
            tempapo = curapogee()
            time.sleep(0.1)
            if (tempapo - curapogee()) < 0:  # if INCREASING, do stuff to fix
                if latepitch == -30:
                    if stagethrottles:
                        if vessel.control.throttle > 0.05:
                            vessel.control.throttle -= 0.05
                    else:
                        latepitch -= 5
                        ap.target_pitch = latepitch
                else:
                    if latepitch > -30:
                        latepitch -= 5
                    ap.target_pitch = latepitch
            else:  # if DECREASING do nothing
                time.sleep(0.001)  # because favorable conds

        ap.wait()

        if dynpress() < 10:
            for b in fairinglist:
                b.jettison()

        # if shoulditstage():
        #     # time.sleep(1)
        #     stagethrottles = canstagethrottle()

        if curperigee() > targetperigee:
            break


def canstagethrottle():
    canthrottle = True
    vessel.control.throttle = 0.05
    time.sleep(0.25)
    minthrust = vessel.thrust
    vessel.control.throttle = 1.00
    if (minthrust / vessel.max_thrust) < .95:
        if debugmode:
            print('Stage can throttle ' + str((minthrust / vessel.max_thrust) * 100) + '%')
        return canthrottle
    else:
        if debugmode:
            print('Stage can not throttle')
        canthrottle = False
        return canthrottle


if __name__ == "__main__":
  main()



