import krpc
import time

# create krpc connection
conn = krpc.connect(name="Planning Hohmann Transfer...")

# create mechjeb object
mj = conn.mech_jeb

print("Planning Hohmann transfer")
planner = mj.maneuver_planner  # create planner object
hohmann = planner.operation_transfer  # create hohmann transfer object
hohmann.make_node()  # actually make the maneuver

# check for warnings
warning = hohmann.error_message
if warning:
    print(warning)

# create executor object
executor = mj.node_executor
executor.tolerance = 3.0
executor.execute_one_node()  # do the thing